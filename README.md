# LawyerP-Box

LawyerP-Box is a keyboard shortcut emulation box. It can be configured to send up to 8 differents shortcuts.

<u>Material list</u><br>
- Micro USB Connector : https://aliexpress.com/item/32827910858.html
- 8 DIP IC Sockets: https://aliexpress.com/item/32226475439.html
- ATTINY85-20PU https://aliexpress.com/item/33040720074.html
- Switch https://aliexpress.com/item/4001226907650.html (MTS-123 Reset)
- Heat Shrinkable tube https://aliexpress.com/item/33046457087.html
- CD4021BE https://aliexpress.com/item/4000934732029.html
- GX12 Aviation Circular Connector 2Pin https://aliexpress.com/item/32816393386.html
- DIP IC Sockets https://aliexpress.com/item/4001252757196.html (16Pin)
- Pedal https://aliexpress.com/item/32970094427.html
- Resistance https://aliexpress.com/item/32224555234.html
- Diodes https://www.amazon.com/dp/B07X9128NY
- Mounting wire https://www.amazon.com/dp/B001IRVDV4
- SPST Switch https://www.smallcab.net/micro-switch-48mm-p-124.html __Needed to replace the pedal one which is SPDT__

<img src="Documentation/LawyerP-Box.jpg" height="200" alt="PCB"/>&nbsp;<img src="Documentation/box1.jpg" height="200" alt="Final product"/>&nbsp;<img src="Documentation/pedal.jpg" height="200" alt="Pedal"/>&nbsp;<img src="Documentation/inside.jpg" height="200" alt="inside"/>

Special Thanks to :<br>
- [tharindurc](https://www.instructables.com/member/tharindurc++/) for the original idea: https://www.instructables.com/DIGIKEYPAD-DigiSpark/
- [MalfatsWats](https://electronics.stackexchange.com/users/91665/malphaswats) for his findings https://electronics.stackexchange.com/a/204211
- [Joey Levy](https://www.youtube.com/channel/UCNhlYr8HaQTKqC6lvZdSpQg) for his tutorial on Micronucleus : https://www.youtube.com/watch?v=o7711jBQxmY

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">LawyerP-Box</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/mike1977/lawyerp-box" property="cc:attributionName" rel="cc:attributionURL">Schenaerts Michaël</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://www.instructables.com/DIGIKEYPAD-DigiSpark/" rel="dct:source">https://www.instructables.com/DIGIKEYPAD-DigiSpark/</a>.
