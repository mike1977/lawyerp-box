#include "shortcut.h"

Shortcut::Shortcut(byte shifterPin, bool mediaKey, uint8_t key1, uint8_t key2, uint8_t modifier) {
      this->shifterPin = shifterPin;
      this->pressed = false;
      this->mediaKey = mediaKey;
      this->key1 = key1;
      this->key2 = key2;
      this->modifier = modifier;
    }
