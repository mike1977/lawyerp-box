#ifndef SHORTCUT_H
#define SHORTCUT_H

#include <TrinketHidCombo.h>
#include <Arduino.h>

class Shortcut {
  public:
    byte shifterPin;
    bool pressed;
    bool mediaKey;
    uint8_t key1;
    uint8_t key2;
    uint8_t modifier;
  
    Shortcut(byte shifterPin, bool mediaKey, uint8_t key1, uint8_t key2, uint8_t modifier);
};
#endif
