#include <TrinketHidCombo.h>
#include "shortcut.h"

/**

  FLASH INSTRUCTION

  If needed flash micronucleus firmware on at85.
  
  Type de carte : ATtiny 25/45/85 (No Bootloader)
  Chip: Attiny85
  Programmateur: Arduino As ISP(ATMega32U4, ATTinyCore)
  
*/

int dataPin = 0;
int clockPin = 1;
int latchPin = 2;

byte RegisterValue = 0;

const unsigned char delayMinValue = 30;
const unsigned char delayMaxValue = 40;

Shortcut pin[8] = {
  Shortcut(B00000001, false, KEYCODE_F, 0, KEYCODE_MOD_LEFT_CONTROL | KEYCODE_MOD_LEFT_GUI | KEYCODE_MOD_LEFT_ALT),
  Shortcut(B00000010, true, MMKEY_PLAYPAUSE, 0, 0),
  Shortcut(B00000100, false, KEYCODE_E, 0, KEYCODE_MOD_LEFT_CONTROL | KEYCODE_MOD_LEFT_GUI | KEYCODE_MOD_LEFT_ALT),
  Shortcut(B00001000, true, MMKEY_SCAN_PREV_TRACK, 0, 0),
  Shortcut(B00010000, false, KEYCODE_A, 0, KEYCODE_MOD_LEFT_CONTROL | KEYCODE_MOD_LEFT_GUI | KEYCODE_MOD_LEFT_ALT),
  Shortcut(B00100000, false, KEYCODE_B, 0, KEYCODE_MOD_LEFT_CONTROL | KEYCODE_MOD_LEFT_GUI | KEYCODE_MOD_LEFT_ALT),
  Shortcut(B01000000, false, KEYCODE_C, 0, KEYCODE_MOD_LEFT_CONTROL | KEYCODE_MOD_LEFT_GUI | KEYCODE_MOD_LEFT_ALT),
  Shortcut(B10000000, false, KEYCODE_D, 0, KEYCODE_MOD_LEFT_CONTROL | KEYCODE_MOD_LEFT_GUI | KEYCODE_MOD_LEFT_ALT)
};

void setup() {

  pinMode(dataPin, INPUT);
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  TrinketHidCombo.begin();

}

void loop() {

  digitalWrite(latchPin, HIGH);
  digitalWrite(clockPin, HIGH); //needed to be able to read first pin
  delayMicroseconds(20);
  digitalWrite(latchPin, LOW);

  RegisterValue = shiftIn(dataPin, clockPin, MSBFIRST);

  for (int i = 0; i < 8; i++) {

    if ((RegisterValue & pin[i].shifterPin) == pin[i].shifterPin) {
      if (!pin[i].pressed) {
        pin[i].pressed = true;
        if (pin[i].mediaKey) {
          TrinketHidCombo.pressMultimediaKey(pin[i].key1);
        } else {
          TrinketHidCombo.pressKey(pin[i].modifier, pin[i].key1, pin[i].key2);
          delay(random(delayMinValue, delayMaxValue));
          TrinketHidCombo.pressKey(0, 0);
        }
      }
    } else {
      TrinketHidCombo.poll();
      pin[i].pressed = false;
    }
  }
  delay(100);

}
